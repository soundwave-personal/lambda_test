import requests
import os


def send_message(msg):
    bot_token = os.environ.get('TELEGRAM_TOKEN')
    bot_chat_id = os.environ.get('TELEGRAM_CHAT_ID')
    print(f'{msg} {bot_token} {bot_chat_id}')

    try:
        send_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + bot_chat_id + '&parse_mode=Markdown&text=' + msg
        response = requests.get(send_text)
        return response.json()
    except Exception as e:
        print("error enviando mensaje por telegram {}".format(e))    
        return False
